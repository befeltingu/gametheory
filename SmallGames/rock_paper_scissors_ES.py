
'''
    Algorithm overview for 2 players

    init policy weights for both players
    init hyper params
    for episode in episodes

'''

import numpy as np

def Fitness(w_n,strat):
    '''
    Calculate ev of strategy of n_th member of population
    against the best strategy of other player
    :param w_n:
    :param strat:
    :return:
    '''
    ev = 0

    for action in range(3):

        action_weight = w_n[action]

        action_ev = 0

        if action == 0: # rock

            action_ev += (-1*strat[1]) # paper

            action_ev += strat[2] # sci


        elif action == 1: # paper

            action_ev += (-1 * strat[2]) # sci

            action_ev += strat[0] # rock


        elif action == 2: # sci

            action_ev += (-1 * strat[0]) # rock

            action_ev += strat[1] # paper

        ev += action_weight*action_ev


    return ev

def update_range(range_hero, max_play, n):

    fraction = 1 - 1 / (n/100 + 2.0)

    for action in range(3):

        if action == max_play:

            range_hero[action] = range_hero[max_play] * (fraction) + (1 - fraction)

        else:
            range_hero[action] = range_hero[action] * (fraction)

    return range_hero

def ESRockPaper():


    # init policies
    #p1 = {"rock": 1.0, "paper": 0.0, "scissors": 0.0}

    p1 = np.array([1.0, 0.0, 0.0])

    #p2 = {"rock": 0.0, "paper": 0.0, "scissors": 1.0}

    p2 = np.array([0.0,0.0,1.0])

    # hyperparameters
    npop = 50  # population size
    sigma = 0.1  # noise standard deviation
    alpha = 0.01  # learning rate

    EPISODES = 2000

    for episode in range(EPISODES):

        # For player 1

        N_1 = np.random.randn(npop, 3)  # sample error from guassian

        R_1 = np.zeros(npop) # init reward vector for each member of the population

        for n in range(N_1.shape[0]):

            w_n = p1 + sigma*N_1[n] # get member of population and perterb by small amount

            w_n = [np.abs(x) for x in w_n] / np.sum(np.abs(w_n))

            R_1[n] = Fitness(w_n,p2)

        # update p1

        update_p1 = alpha*(1.0/ (npop * sigma)) * np.dot(N_1.T,R_1)

        max_play = np.argmax(update_p1)

        p1 = update_range(p1,max_play,episode)

        # For player 2

        N_2 = np.random.randn(npop, 3)  # sample error from guassian

        R_2 = np.zeros(npop)  # init reward vector for each member of the population

        for n in range(N_2.shape[0]):

            w_n = p2 + sigma * N_2[n]  # get member of population and perterb by small amount

            w_n = [np.abs(x) for x in w_n] / np.sum(np.abs(w_n))

            R_2[n] = Fitness(w_n, p1)

        # update p1

        update_p2 = alpha * (1.0 / (npop * sigma)) * np.dot(N_2.T, R_2)

        max_play_2 = np.argmax(update_p2)

        p2 = update_range(p2,max_play_2,episode)



    print(str(p1) + "\n")

    print(str(p2))


ESRockPaper()



