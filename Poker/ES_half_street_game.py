
import random
import numpy as np
from Util.node import InfoNode, PokerNode
from Util.tree import InfoTree
import random
import gym
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam



class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.gamma = 0.95  # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.1
        self.model = self._build_model()

    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
        model.add(Dense(12, input_dim=self.state_size, activation='relu',use_bias=False))
        #model.add(Dense(self.action_size, input_dim=self.state_size, activation='linear',use_bias=False))
        model.add(Dense(2, activation='linear',use_bias=False))
        model.compile(loss='mse',
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def act(self, state):
        act_values = self.model.predict(state)

        return act_values  # returns action

class AKQPlayer(object):
    def __init__(self, name, info_tree, starting_stack):
        self.name = name

        self.info_tree = info_tree

        self.policy = {}

        self.out_of_tree = False

        self.current_hand = None

        self.starting_stack = starting_stack

class ESAgentHalfStreet(object):
    '''
        class used for simulating a simple AKQ poker game
        The game state needs to deal random cards to each player
        and to track the button
    '''

    def __init__(self, game_tree):

        self.player1 = None

        self.player2 = None

        self.sb_dqn_agent = None

        self.bb_dqn_agent = None

        self.mix_param = 0  # mixing parameter used for selecting actions

        self.iter_count = 0

        self.deck = [3, 2, 1]

        self.actions = ["bet", "check"]

        self.game_tree = game_tree  # the full game tree for for the information trees to reference

        self.replay_data = []

        self.behavior_policy = {
            "SB": {},
            "BB": {}
        }

        self.current_policy = 'normal'  # current policy being used

        self.memory_size = 30000

        self.init_game()

    def init_game(self):

        SB_tree = InfoTree()  # info tree

        chance_node = PokerNode(player="chance", SB_cip=0.0, BB_cip=0.0)

        SB_tree.set_root(chance_node)

        BB_tree = InfoTree()  # info tree

        game_root = self.game_tree.get_root()

        BB_root_node = PokerNode(game_root.player, SB_cip=0, BB_cip=0)

        BB_tree.set_root(BB_root_node)

        self.player1 = AKQPlayer(name="SB", info_tree=SB_tree, starting_stack=1)

        self.player2 = AKQPlayer(name="BB", info_tree=BB_tree, starting_stack=1)

        # self.player1.policy[0] = {}

        # self.player2.policy[0] = {}

        for curr_node in self.game_tree.nodes:

            if curr_node.player == 'Leaf':
                continue

            actions = [list(node.action.keys())[0] for node in curr_node.children]

            self.behavior_policy[curr_node.player][curr_node.node_index] = {}

            current_player = self.player1 if curr_node.player == "SB" else self.player2

            current_player.policy[curr_node.node_index] = 0

            for action in actions:
                self.behavior_policy[curr_node.player][curr_node.node_index][action] = {'A': 0.0, 'K': 0.0, 'Q': 0.0}

        sb_dqn = DQNAgent(3, 2)

        bb_dqn = DQNAgent(3, 2)

        self.sb_dqn_agent = sb_dqn

        self.bb_dqn_agent = bb_dqn

    def deal_hand(self):

        return random.choice(self.deck)

    def flatten_weights(self, weights):

        tots_weights = np.array([])

        for layer in weights:
            tots_weights = np.append(tots_weights, layer)

        return tots_weights

    def get_hand_string(self, hand):

        if hand == 3:
            return "A"

        elif hand == 2:
            return "K"

        else:
            return "Q"

    def get_hero_villian(self, s):

        '''
        Take in the name of the owner of the current node and returns hero,villian
        :param current_player:
        :return: []
        '''

        if s.parent.player == "SB":

            return [self.player1, self.player2]

        else:

            return [self.player2, self.player1]

    def get_hero_villian_cip(self, s):

        if s.parent.player == "SB":

            return [s.SB_cip, s.BB_cip]

        else:
            return [s.BB_cip, s.SB_cip]

    def get_legal_actions(self,s):

        p_actions = []
        for c in s.children:
            action_type = list(c.action.keys())[0]
            p_actions.append(self.actions.index(action_type))

        return p_actions

    def get_info_state(self, current_player, s):

        '''

        info state for the AKG game is (actions,hand)
        actions are all previous actions to this node

        For the AKQ I dont think we need to keep track of
        actions so the info stat is only going to have
        the additional hand value

        Append
        :param s:
        :return:

        '''

        if s.parent == None:
            # this is a root node and the parent will be chance

            NewInfoNode = InfoNode(current_player.current_hand, player=s.player, action=s.action,
                                   parent=current_player.info_tree.get_root(),
                                   SB_cip=s.SB_cip, BB_cip=s.BB_cip, is_leaf=s.is_leaf)

            return NewInfoNode

        # need to make a copy of the parent cannot pass as reference

        new_parent = PokerNode(s.parent.player, parent=s.parent.parent, SB_cip=s.parent.SB_cip, BB_cip=s.parent.BB_cip,
                               action=s.parent.action)

        NewInfoNode = InfoNode(current_player.current_hand, player=s.player, action=s.action, parent=new_parent,
                               SB_cip=s.SB_cip, BB_cip=s.BB_cip, is_leaf=s.is_leaf)

        return NewInfoNode

    def get_new_state(self, s, a):

        node_children = s.children

        for child in node_children:

            if child.action == a:

                return child

            else:
                continue

        # we should not reach this line of code
        # the function should always be able to return a new state

        raise Exception("get_new_state was not able to find a child with the given action")

    def get_child_info(self, s, a):

        for child in s.children:
            if s.action == a:
                return child

        # should not reach this location

        raise Exception("g_child_info parent does not have child with action: " + str(a))

    def get_weight_shape(self, weights):

        weights_shape = []

        for layer in weights:
            weights_shape.append((layer.shape, layer.size))

        return weights_shape

    def reshape_weights(self, weights, shape):

        reshaped_weights = []

        last_index = 0

        for shape, size in shape:
            curr_layer = weights[last_index:last_index + size]

            reshaped_weights.append(np.reshape(curr_layer, shape))

            last_index += size

        return reshaped_weights

    def reward(self, s):

        '''

        Takes in a leaf node and returns the reward to each player
        :param s:
        :return:

        '''

        r = {"SB": 0, "BB": 0}

        hero, villian = self.get_hero_villian(s)

        hero_cip, villian_cip = self.get_hero_villian_cip(s)

        current_pot = 1.0 + s.SB_cip + s.BB_cip

        action_type = list(s.action.keys())[0]

        if action_type == "check": # for half street game checking returns award of 0 to each player

            # evaluate winner
            if (hero.current_hand > villian.current_hand):
                # SB wins
                r[hero.name] = current_pot + (hero.starting_stack - hero_cip)

                r[villian.name] = villian.starting_stack - villian_cip

            else:

                r[villian.name] = current_pot + (villian.starting_stack - villian_cip)

                r[hero.name] = hero.starting_stack - hero_cip



        elif action_type == "call":  # same as check?

            # evaluate winner
            if (hero.current_hand > villian.current_hand):
                # SB wins
                r[hero.name] = current_pot + (hero.starting_stack - hero_cip)

                r[villian.name] = villian.starting_stack - villian_cip

            else:

                r[villian.name] = current_pot + (villian.starting_stack - villian_cip)

                r[hero.name] = hero.starting_stack - hero_cip

        return r

    def get_state_vector(self, s, player):

        '''
        Takes in the node and current hand of the player and
        return the array representation of the current state

        state = players x raises x actions + card_vector
        :param s:
        :return:
        '''



        hand_array = np.zeros(3)

        # curr_hand_index = self.deck.index(player.current_hand)

        hand_array[player.current_hand - 1] = 1

        state = np.reshape(hand_array, [1, 3])

        return state

    def select_action(self, s):

        if s.player == "BB":
            return {'call':1}

        current_player = self.player1 if s.player == "SB" else self.player2

        current_agent = self.sb_dqn_agent if s.player == "SB" else self.bb_dqn_agent

        current_state = self.get_state_vector(s, current_player)

        possible_actions = current_agent.act(current_state)[0]

        possible_actions = list(map(abs,possible_actions))

        possible_actions = [action / sum(possible_actions) for action in possible_actions]

        #choose_action = np.random.choice(legal_actions_string, 1, p=possible_actions)[0]

        choose_action = np.argmax(possible_actions)

        choose_action = self.actions[choose_action]


        if choose_action == "check":

            return {choose_action: 0}
        else:
            return {choose_action: 1}

    def simulate(self, s):

        self.iter_count += 1

        '''

            Takes in a state

            if state.terminal == True:
                return reward

            Player = player(s)
            if Player.out_of_tree == True:
                return rollout(s)
            InfoState = information_function(s) maps state to info state
            if InfoState not in PlayerTree:
                Expand(PlayerTree,InfoState)
                a = rollout_policy
                Player.out_of_tree = True
            else:
                a = select(InfoState)
            s' = G(s,a)
            r = simulate(s')
            update(InfoState,a,r)
            return r

        '''

        if s.is_leaf == True:
            return self.reward(s)

        current_player = self.player1 if s.player == "SB" else self.player2

        if current_player.out_of_tree == True:

            return self.rollout(s)

        action = self.select_action(s)

        next_state = self.get_new_state(s, action)

        ###############
        # REPLAY DATA
        ###############

        action_type = list(action.keys())[0]

        r = self.simulate(next_state)

        return r

    def run(self, num_iterations, current_player, policy='br_policy'):

        self.current_policy = policy

        total_reward = 0

        for i in range(num_iterations):
            # self.iter_count = i

            self.deck = [3, 2, 1]  # reshuffle the cards yo

            # deals cards to each player

            sb_card = self.deal_hand()

            self.player1.current_hand = sb_card

            self.deck.remove(sb_card)

            bb_card = self.deal_hand()

            self.player2.current_hand = bb_card

            s0 = self.game_tree.get_root()

            r = self.simulate(s0)

            total_reward += r[current_player]

        return total_reward / float(num_iterations)

    # update the run function in mcts_akq
    def run_ES(self, num_iterations, population=50):

        sigma = 0.1  # noise standard deviation

        alpha = 0.001  # learning rate

        sb_init_params = self.sb_dqn_agent.model.get_weights()

        sb_init_weights_flat = self.flatten_weights(sb_init_params)

        weights_shape = self.get_weight_shape(sb_init_params)

        current_sb_weights = sb_init_weights_flat

        for j in range(num_iterations):

            print("Running iteration: " + str(j))

            # First do the small blind

            F = np.zeros(population)

            N = np.random.randn(population, len(sb_init_weights_flat))

            for p in range(population):
                p_w = current_sb_weights

                p_w += sigma * N[p]

                p_w = self.reshape_weights(p_w,weights_shape)  # reformat to look like original in order ot place them on the model

                self.sb_dqn_agent.model.set_weights(p_w)  # Now set the weights for the model

                F[p] = self.run(10, "SB")

            #A = (F - np.mean(F)) / np.std(F)

            print("MEAN reward for SB: " + str(np.mean(F)))

            current_sb_weights += alpha / (population * sigma) * np.dot(N.T, F)  # update the flattened weights

            self.sb_dqn_agent.model.set_weights(self.reshape_weights(current_sb_weights, weights_shape))

        return [self.player1.policy, self.player2.policy]


