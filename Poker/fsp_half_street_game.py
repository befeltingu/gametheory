

import random
import numpy as np
from Util.node import InfoNode,PokerNode
from Util.tree import InfoTree
import random
import gym
import numpy as np
import pandas as pd
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam


class DQNAgent:

    def __init__(self, state_size, action_size):

        self.state_size = state_size
        self.action_size = action_size
        self.batch_size = 32
        self.memory = deque(maxlen=20000)
        self.memory = []
        self.gamma = 0.95  # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.655
        self.learning_rate = 0.01
        self.model = self._build_model()

    def _build_model(self):

        # Neural Net for Deep-Q learning Model
        model = Sequential()
        #model.add(Dense(48, input_dim=self.state_size, activation='relu'))
        model.add(Dense(12, input_dim=self.state_size, activation='relu'))
        model.add(Dense(self.action_size, activation='linear'))
        model.compile(loss='mse',
                      optimizer=Adam(lr=self.learning_rate))
        return model

    def remember(self, state, action, reward, next_state, done):

        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):

        if np.random.rand() <= self.epsilon:
            action_index = np.random.randint(self.action_size)
            return action_index

        else:

            act_values = self.model.predict(state)

            return np.argmax(act_values)  # returns action'''

        #act_values = self.model.predict(state)

        #return np.argmax(act_values)  # returns action

    def replay(self):

        if len(self.memory) < self.batch_size:
            return

        minibatch = random.sample(self.memory, self.batch_size)

        Q_ = self.model.predict(np.array([[1,0,0]]))
        K_ = self.model.predict(np.array([[0,1,0]]))
        A_ = self.model.predict(np.array([[0,0,1]]))



        for state, action, reward, next_state, done in minibatch:

            target = reward*0.25

            target_f = self.model.predict(state)

            target_f[0][action] = target

            self.model.fit(state, target_f, epochs=1, verbose=0)

        if self.epsilon > self.epsilon_min:

            self.epsilon *= self.epsilon_decay

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)

class AKQPlayer(object):

    def __init__(self,name,info_tree,starting_stack):

        self.name = name

        self.info_tree = info_tree

        self.policy = {}

        self.out_of_tree = False

        self.current_hand = None

        self.starting_stack = starting_stack

class FSPHalfStreetAgent(object):

    '''
        class used for simulating a simple AKQ poker game
        The game state needs to deal random cards to each player
        and to track the button
    '''

    def __init__(self,game_tree,conn):

        self.player1 = None

        self.player2 = None

        self.sb_dqn_agent = None

        self.bb_dqn_agent = None

        self.mix_param = 0 # mixing parameter used for selecting actions

        self.iter_count = 0

        self.deck = [3,2,1]

        self.actions = ["bet", "check"]

        self.game_tree = game_tree # the full game tree for for the information trees to reference

        self.replay_data = []

        self.adaptive_constant  = 0.8

        self.behavior_policy = {
            "SB": {},
            "BB":{}
        }

        self.current_policy = 'normal' # current policy being used

        self.memory_size = 30000

        self.db_connection = conn

        self.init_game()

    def init_game(self):

        SB_tree = InfoTree() # info tree

        chance_node = PokerNode(player="chance",SB_cip=0.0,BB_cip=0.0)

        SB_tree.set_root(chance_node)

        BB_tree = InfoTree() # info tree

        game_root = self.game_tree.get_root()

        BB_root_node = PokerNode(game_root.player,SB_cip=0,BB_cip=0)

        BB_tree.set_root(BB_root_node)

        self.player1 = AKQPlayer(name="SB",info_tree=SB_tree,starting_stack=1)

        self.player2 = AKQPlayer(name="BB",info_tree=BB_tree,starting_stack=1)

        pd_test = pd.DataFrame({"bet": np.zeros(3), "check": np.zeros(3),"bet_policy":np.zeros(3),"check_policy":np.zeros(3)})

        pd_test = pd_test.rename({0:"A",1:"K",2:"Q"})

        self.sb_policy = pd_test

        sb_dqn = DQNAgent(3, 2)

        self.sb_dqn_agent = sb_dqn

    def convert_memory_to_frame(self):
        '''
        convert dqn memory to a dataframe
        :return:
        '''

        #current_node_vector, action, r, next_node_vector, next_state.is_leaf
        columns = ["current_hand","action","reward"]
        formated_list = []
        for state, action, reward, next_state, done in self.sb_dqn_agent.memory:

            current_hand = self.get_hand_string(np.where(state[0] == 1)[0][0] + 1)

            current_action = self.actions[action]

            formated_list.append([current_hand,current_action,reward])


        memory_df = pd.DataFrame(formated_list,columns=columns)

        return memory_df

        print("done converting to dataframe")

    def deal_hand(self):

        return random.choice(self.deck)

    def get_hand_string(self,hand):

        if hand == 3:
            return "A"

        elif hand == 2:
            return "K"

        else:
            return "Q"

    def get_hero_villian(self,s):

        '''
        Take in the name of the owner of the current node and returns hero,villian
        :param current_player:
        :return: []
        '''

        if s.parent.player == "SB":

            return [self.player1,self.player2]

        else:

            return [self.player2,self.player1]

    def get_hero_villian_cip(self, s):

        if s.parent.player == "SB":

            return [s.SB_cip, s.BB_cip]

        else:
            return [s.BB_cip, s.SB_cip]

    def get_new_state(self, s, a):

        node_children = s.children

        for child in node_children:

            if child.action == a:

                return child

            else:
                continue

        # we should not reach this line of code
        # the function should always be able to return a new state

        raise Exception("get_new_state was not able to find a child with the given action")

    def reward(self,s):

        '''

        Takes in a leaf node and returns the reward to each player
        :param s:
        :return:

        '''

        r = {"SB":0,"BB":0}

        hero, villian = self.get_hero_villian(s)

        hero_cip, villian_cip = self.get_hero_villian_cip(s)

        current_pot = 2.0 + s.SB_cip + s.BB_cip

        action_type = list(s.action.keys())[0]

        if action_type == "fold":
            # the parent folded so the current player gets the pot
            r[hero.name] = hero.starting_stack - hero_cip

            r[villian.name] = current_pot + (villian.starting_stack - villian_cip)


        elif action_type == "check":

            # evaluate winner
            if (hero.current_hand > villian.current_hand):
                # SB wins
                r[hero.name] = current_pot + (hero.starting_stack - hero_cip)

                r[villian.name] = villian.starting_stack - villian_cip

            else:

                r[villian.name] = current_pot + (villian.starting_stack - villian_cip)

                r[hero.name] = hero.starting_stack - hero_cip


        elif action_type == "call": # same as check?

            # evaluate winner
            if (hero.current_hand > villian.current_hand):
                # SB wins
                r[hero.name] = current_pot + (hero.starting_stack - hero_cip)

                r[villian.name] = villian.starting_stack - villian_cip

            else:

                r[villian.name] = current_pot + (villian.starting_stack - villian_cip)

                r[hero.name] = hero.starting_stack - hero_cip

        return r

    def store_transition(self,current_player,s,action,r,next_state):

        current_node_vector = self.get_state_vector(s,current_player)

        next_node_vector = self.get_state_vector(next_state,current_player)

        if current_player.name == "SB":

            self.sb_dqn_agent.remember(current_node_vector, action, r, next_node_vector, next_state.is_leaf)

    def get_state_vector(self,s,current_player):

        '''
                Takes in the node and current hand of the player and
                return the array representation of the current state

                state = players x raises x actions + card_vector
                :param s:
                :return:
                '''

        hand_array = np.zeros(3)

        # curr_hand_index = self.deck.index(player.current_hand)

        hand_array[current_player.current_hand - 1] = 1

        state = np.reshape(hand_array, [1, 3])

        return state

    def select_action(self,u_i):

        '''
            if policy = best response:

                a ~ max_a Q(s,a)

            else:
                policy = N(u,a) / N(u)
                return a ~ p
        '''
        if u_i.node_index == 1:

            return {"call":1}

        N_U = u_i.count

        #if u_i.node_index != 0 and u_i.node_index != 1:
        #    print("wtf")

        if self.current_policy == 'br_policy':

            if N_U == 0:

                print("Visit count = 0!")

            current_max_action = None

            current_max = -1

            current_player = self.player1 if u_i.player == "SB" else self.player2

            current_agent = self.sb_dqn_agent if u_i.player == "SB" else self.bb_dqn_agent

            current_state = self.get_state_vector(u_i,current_player)

            max_action_index = current_agent.act(current_state)

            #max_action_index = np.argmax(possible_actions)

            current_max_action = self.actions[max_action_index]

            '''info_policy = current_player.policy[u_i.node_index]

            for action in info_policy.keys():

                action_index = self.actions.index(action)

                score = possible_actions[action_index]

                if score > current_max:

                    current_max = score

                    current_max_action = action'''

            if current_max_action == "check" or current_max_action == "fold":

                return {current_max_action: 0}
            else:
                return {current_max_action: 1}

        elif self.current_policy == 'normal':

            current_player = self.player1 if u_i.player == "SB" else self.player2

            action_p = []

            actions = ['bet','check']

            '''for action in current_player.policy[u_i.node_index].keys():

                n_a_count = current_player.policy[u_i.node_index][action]['count']

                action_p.append(float(n_a_count/N_U))

                actions.append(action)

            choose_action = np.random.choice(actions,1,p=action_p)[0]'''

            current_hand_string = self.get_hand_string(current_player.current_hand)

            bet_prob = self.sb_policy.get_value(current_hand_string,'bet_policy')

            check_prob = self.sb_policy.get_value(current_hand_string,'check_policy')

            action_p.append(bet_prob)

            action_p.append(check_prob)

            choose_action = np.random.choice(actions, 1, p=action_p)[0]

            if choose_action == "check" or choose_action == "fold":

                return {choose_action: 0}
            else:
                return {choose_action: 1}

        else:
            print("ERROR Unknown policy used: " + str(self.current_policy))

    def simulate(self,s):

        '''
            For the half street game this is the easiest thing ever..... hopefully
            just evaluate

        '''

        if s.is_leaf == True:

            return self.reward(s)

        current_player = self.player1 if s.player == "SB" else self.player2

        action = self.select_action(s)

        next_state = self.get_new_state(s, action)

        ###############
        # REPLAY DATA
        ###############

        action_type = list(action.keys())[0]

        r = self.simulate(next_state)

        if s.player == "SB":

            action_index = self.actions.index(action_type)

            self.store_transition(current_player, s, action_index, r[current_player.name], next_state)

            if self.current_policy == "br_policy":

                self.update(current_player,s,action,r)

        return r

    def update(self,current_player,s, a, r):

        '''
        N(u_i) += 1
        N(u,a) += 1
        Q(u,a) += (r - Q(u,a)) / N(u,a)
        '''

        s.count += 1

        action_type = list(a.keys())[0]

        #current_player.policy[.node_index][action_type]['count'] += 1

        # update policy simply N(u,a) / N(u)

        string_hand = self.get_hand_string(current_player.current_hand)

        self.sb_policy.ix[string_hand,action_type] += 1

        N_U_I = self.sb_policy.ix[string_hand,'bet'] + self.sb_policy.ix[string_hand,'check']

        for child in s.children:

            child_action = list(child.action.keys())[0]

            N_U_A = self.sb_policy.ix[string_hand,child_action]

            self.sb_policy.ix[string_hand, child_action + "_policy"] = N_U_A / N_U_I

    def update_rl(self,iter):

        # fist do sb

        if (iter % 50 == 0):

            memory_df = self.convert_memory_to_frame()

            save_path = '/Users/befeltingu/GameTheory/DB/fsp_half_street/memory_df'

            memory_df.to_hdf(save_path,key=str(iter),mode='w')

            prob_df = self.get_state_value_df()

            save_path = '/Users/befeltingu/GameTheory/DB/fsp_half_street/value_df'

            prob_df.to_hdf(save_path,key=str(iter),mode='w')

        self.sb_dqn_agent.replay()

    def get_mixing_parameter(self,j, n, m):
        '''
            In the paper for their first experiment they used. mix_param = alpha / 10*p where p = n * m / memory size
            I think alpha was used as 1/k?
        '''

        p = (n + m) / self.memory_size

        #return (1/(10 * j * p))
        return 1.0 / j

    def get_state_value_df(self):

        states = [[1,0,0],[0,1,0],[0,0,1]]

        states_values = []

        for s in states:

            hand = self.get_hand_string(s.index(1) + 1)
            s = np.reshape(s,(1,3))
            state_value = self.sb_dqn_agent.model.predict(s)
            states_values.append([hand] + list(state_value[0]))

        state_df = pd.DataFrame(states_values,columns=["hand","bet_value","check_value"])

        return state_df

    def run(self,num_iterations,policy='normal'):

        self.current_policy = policy

        for i in range(num_iterations):

            #self.iter_count = i

            self.deck = [3,2,1] # reshuffle the cards yo

            self.player1.out_of_tree = False

            self.player2.out_of_tree = False

            # deals cards to each player

            sb_card = self.deal_hand()

            self.player1.current_hand = sb_card

            self.deck.remove(sb_card)

            bb_card = self.deal_hand()

            self.player2.current_hand = bb_card

            s0 = self.game_tree.get_root()

            self.simulate(s0)

    # update the run function in mcts_akq
    def run_fsp(self, num_iterations, n=2, m=1, k=30):

        for j in range(2, num_iterations):

            mix_param = self.get_mixing_parameter(j,n,m)

            self.mix_param = mix_param

            self.sb_dqn_agent.memory = []

            self.generate_data(k,n,m)

            self.update_rl(j - 2)

    def generate_data(self, k, n, m):

        for s in range(m):
            self.run(num_iterations=k,policy='br_policy')

        for s in range(n):
            self.run(num_iterations=k,policy='normal')
