from Poker.XFP import StrategyProfile,FP
from Poker.mcts_akq import AKQGameState
from Poker.mcts_behavior import AKQMixedMcts,MCTSStrategyProfile
from Poker.NFSP_simple import NFSPSimple
from Poker.FSP import FSPAgent
from Poker.mcts_dqn import MCTSDQN
from Poker.ES_akq import ESAgent
from Poker.ES_akq_policy import ESAgentPolicy
from Poker.ES_half_street_game import ESAgentHalfStreet
from Poker.fsp_half_street_game import FSPHalfStreetAgent
from Graph.graph_tree import TreeGraph
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam

import graphviz as gv
import pandas as pd
import Util.tree as Tree
import Poker.game as Game
import numpy as np
import sqlite3,json,io

def adapt_array(arr):
    """
    http://stackoverflow.com/a/31312102/190597 (SoulNibbler)
    """
    out = io.BytesIO()
    np.save(out, arr)
    out.seek(0)
    return sqlite3.Binary(out.read())

def convert_array(text):
    out = io.BytesIO(text)
    out.seek(0)
    return np.load(out)


###############################
## Test StrategyProfile: PASS #
###############################
run_test_strategy_prof = 0
if run_test_strategy_prof:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    akq_game.new_action(current_index=2, player="BB", action={"bet": 1})
    akq_game.new_action(current_index=2, player="BB", action={"check": 0})

    akq_game.new_action(current_index=5, player="SB", action={"call": 1})
    akq_game.new_action(current_index=5, player="SB", action={"fold": 0})

    #strategy_profile = StrategyProfile(akq_game.tree)


    print("Done testing Strat profile")

#####################################
## Run AKQ XFP Algo For simple tree #
#####################################
run_akq_xfp_simple = 0
if run_akq_xfp_simple:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"bet": 1})
    akq_game.new_action(current_index=1, player="BB", action={"check": 0})

    akq_game.new_action(current_index=2, player="SB", action={"call": 1})
    akq_game.new_action(current_index=2, player="SB", action={"fold": 0})

    solution_strategies = FP(akq_game.tree,n_iter=1000)

    print("Done running akq xfp")

#####################################
## Run AKQ XFP Algo Longer version  #
#####################################
run_akq_xfp = 0
if run_akq_xfp:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    akq_game.new_action(current_index=2, player="BB", action={"bet": 1})
    akq_game.new_action(current_index=2, player="BB", action={"check": 0})

    akq_game.new_action(current_index=5, player="SB", action={"call": 1})
    akq_game.new_action(current_index=5, player="SB", action={"fold": 0})

    solution_strategies = FP(akq_game.tree,n_iter=100000)

    print("Done running akq xfp")

########################################
## Run MCTS akq tree. Version: regular #
########################################
run_mcts_akq_regular = 0
if run_mcts_akq_regular:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    akq_game.tree.nodes[2].is_leaf = True

    GameState = AKQGameState(tree)

    new_graph = gv.Digraph(format="png")

    AKQGraph = TreeGraph(tree=akq_game.tree, graph=new_graph)

    AKQGraph.create_graph_from_tree()

    AKQGraph.graph.render('/Users/befeltingu/GameTheory/Results/Poker/MCTS/img/akq_extended')

    p1_policy, p2_policy = GameState.run(100000)

    replay_data_df = pd.DataFrame(GameState.replay_data)

    p1_ev_matrix = []

    for node in GameState.player1.info_tree.nodes:

        if node.player == "chance":
            continue

        current_hand = node.player_hand

        policy = p1_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p1_ev_matrix.append(
                ['player 1', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_1 = pd.DataFrame(p1_ev_matrix)

    p2_ev_matrix = []

    for node in GameState.player2.info_tree.nodes:

        if node.player != "p2":
            continue

        current_hand = node.player_hand

        policy = p2_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p2_ev_matrix.append(
                ['player 2', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_2 = pd.DataFrame(p2_ev_matrix)

    ev_df_1.to_csv('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/ev_1.csv')

    ev_df_2.to_csv('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/ev_2.csv')

####################################
## Run MCTS mixed. Version: small  #
####################################
run_mcts_akq_regular = 0
if run_mcts_akq_regular:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"bet": 1})
    akq_game.new_action(current_index=1, player="BB", action={"check": 0})

    akq_game.new_action(current_index=2, player="SB", action={"call": 1})
    akq_game.new_action(current_index=2, player="SB", action={"fold": 0})

    strategy_profile = MCTSStrategyProfile(tree)

    GameState = AKQMixedMcts(tree,strategy_profile)

    new_graph = gv.Digraph(format="png")

    AKQGraph = TreeGraph(tree=akq_game.tree, graph=new_graph)

    AKQGraph.create_graph_from_tree()

    AKQGraph.graph.render('/Users/befeltingu/GameTheory/Results/Poker/MCTS/img/akq_extended')

    strategy_profile = GameState.run(1000)

    p1_ev_matrix = []

    for node in GameState.player1.info_tree.nodes:

        if node.player == "chance":
            continue

        current_hand = node.player_hand

        policy = p1_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p1_ev_matrix.append(
                ['player 1', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_1 = pd.DataFrame(p1_ev_matrix)

    p2_ev_matrix = []

    for node in GameState.player2.info_tree.nodes:

        if node.player != "p2":
            continue

        current_hand = node.player_hand

        policy = p2_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p2_ev_matrix.append(
                ['player 2', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_2 = pd.DataFrame(p2_ev_matrix)

    ev_df_1.to_csv('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/ev_1.csv')

    ev_df_2.to_csv('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/ev_2.csv')

####################################
## Run NFSP simple                 #
####################################
run_nfsp_simple = 0
if run_nfsp_simple:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    #akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    tree.nodes[2].is_leaf = True

    new_graph = gv.Digraph(format="png")

    #Graph = TreeGraph(tree=akq_game.tree, graph=new_graph)

    #Graph.create_graph_from_tree()

    #Graph.graph.render('/Users/befeltingu/GameTheory/Results/Poker/MCTS/img/nsfp')

    nfsp_simple = NFSPSimple(tree)

    policy,sb_DQN,bb_DQN = nfsp_simple.run(150)

    replay_data_df = pd.DataFrame(nfsp_simple.replay_list)

    max_action_final = {"SB": { "A":None,"K":None,"Q":None}, "BB": { "A":None,"K":None,"Q":None}}


    for hand in [0,1,2]:

        nfsp_simple.player1.current_hand = hand

        current_state = nfsp_simple.get_state_from_node(nfsp_simple.tree.nodes[2],nfsp_simple.player1)

        valid_actions = [0,1] # bet and check

        max_action = sb_DQN.act(current_state,valid_actions)[0]

        hand_string = nfsp_simple.get_hand_string(hand)

        max_action_final["SB"][hand_string] = max_action


    for hand in [0,1,2]:

        nfsp_simple.player2.current_hand = hand

        current_state = nfsp_simple.get_state_from_node(nfsp_simple.tree.nodes[1], nfsp_simple.player2)

        valid_actions = [2, 3]  # bet and check

        max_action = bb_DQN.act(current_state, valid_actions)[0]

        hand_string = nfsp_simple.get_hand_string(hand)

        max_action_final["BB"][hand_string] = max_action







    print("done running nfsp simple")

####################################
## Run MCTS DQN                    #
####################################
run_mcts_dqn = 0
if run_mcts_dqn:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    tree.nodes[2].is_leaf = True

    new_graph = gv.Digraph(format="png")

    mcts_dqn = MCTSDQN(tree)

    p1_policy,p2_policy = mcts_dqn.run(10)

    #replay_data_df = pd.DataFrame(mcts_dqn.replay_data)

    np.save('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/replay_history_array_sb.npy',mcts_dqn.replay_data_SB)

    np.save('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/replay_history_array_bb.npy',mcts_dqn.replay_data_BB)

    #replay_save_path = '/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/replay_history.csv'

    #replay_data_df.to_csv(replay_save_path)

    p1_ev_matrix = []

    for node in mcts_dqn.player1.info_tree.nodes:

        if node.player == "chance":
            continue

        current_hand = node.player_hand

        policy = p1_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p1_ev_matrix.append(
                ['player 1', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_1 = pd.DataFrame(p1_ev_matrix)

    p2_ev_matrix = []

    for node in mcts_dqn.player2.info_tree.nodes:

        if node.player != "p2":
            continue

        current_hand = node.player_hand

        policy = p2_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p2_ev_matrix.append(
                ['player 2', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_2 = pd.DataFrame(p2_ev_matrix)



    print("done running nfsp simple")

###############################
## Run FSP                    #
###############################
run_fsp = 0
if run_fsp:

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    tree.nodes[2].is_leaf = True

    new_graph = gv.Digraph(format="png")

    fsp = FSPAgent(tree)

    p1_policy,p2_policy = fsp.run_fsp(200)

    #replay_data_df = pd.DataFrame(mcts_dqn.replay_data)

    #np.save('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/replay_history_array_sb.npy',mcts_dqn.replay_data_SB)

    #np.save('/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/replay_history_array_bb.npy',mcts_dqn.replay_data_BB)

    #replay_save_path = '/Users/befeltingu/GameTheory/Results/Poker/MCTS/data/replay_history.csv'

    #replay_data_df.to_csv(replay_save_path)

    p1_ev_matrix = []

    for node in fsp.player1.info_tree.nodes:

        if node.player == "chance":
            continue

        current_hand = node.player_hand

        policy = p1_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p1_ev_matrix.append(
                ['player 1', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_1 = pd.DataFrame(p1_ev_matrix)

    p2_ev_matrix = []

    for node in fsp.player2.info_tree.nodes:

        if node.player != "p2":
            continue

        current_hand = node.player_hand

        policy = p2_policy[node.node_index]

        for action in policy.keys():
            ev = policy[action]['ev']

            p2_ev_matrix.append(
                ['player 2', 'node: ' + str(node.node_index), 'hand:' + str(current_hand), action, 'value: ' + str(ev)])

    ev_df_2 = pd.DataFrame(p2_ev_matrix)



    print("done running nfsp simple")

###############################
## Run Simple ES              #
###############################
run_simple_es = 0
if run_simple_es:
    '''
    copied from this repo https://gist.github.com/karpathy/77fbb6a8dac5395f1b73e7a89300318d
    '''
    """
    A bare bones examples of optimizing a black-box function (f) using
    Natural Evolution Strategies (NES), where the parameter distribution is a
    gaussian of fixed standard deviation.
    """

    import numpy as np

    np.random.seed(0)


    # the function we want to optimize
    def f(w):
        # here we would normally:
        # ... 1) create a neural network with weights w
        # ... 2) run the neural network on the environment for some time
        # ... 3) sum up and return the total reward
        # but for the purposes of an example, lets try to minimize
        # the L2 distance to a specific solution vector. So the highest reward
        # we can achieve is 0, when the vector w is exactly equal to solution
        reward = -np.sum(np.square(solution - w))
        return reward


    # hyperparameters
    npop = 50  # population size
    sigma = 0.1  # noise standard deviation
    alpha = 0.001  # learning rate

    # start the optimization
    solution = np.array([0.5, 0.1, -0.3])
    w = np.random.randn(3)  # our initial guess is random

    for i in range(300):

        # print current fitness of the most likely parameter setting
        if i % 20 == 0:
            print('iter %d. w: %s, solution: %s, reward: %f' %
                  (i, str(w), str(solution), f(w)))

        # initialize memory for a population of w's, and their rewards
        N = np.random.randn(npop, 3)  # samples from a normal distribution N(0,1)
        R = np.zeros(npop)
        for j in range(npop):

            w_try = w + sigma * N[j]  # jitter w using gaussian of sigma 0.1

            R[j] = f(w_try)  # evaluate the jittered version

        # standardize the rewards to have a gaussian distribution
        A = (R - np.mean(R)) / np.std(R)
        # perform the parameter update. The matrix multiply below
        # is just an efficient way to sum up all the rows of the noise matrix N,
        # where each row N[j] is weighted by A[j]
        w = w + alpha / (npop * sigma) * np.dot(N.T, A)

#######################
## Run Test DQN       #
#######################
run_test_dqn = 0
if run_test_dqn:

    class DQNAgent:

        def __init__(self, state_size, action_size):

            self.state_size = state_size
            self.action_size = action_size
            self.memory = deque(maxlen=200)
            self.gamma = 1  # discount rate
            self.epsilon = 1.0  # exploration rate
            self.epsilon_min = 0.01
            self.epsilon_decay = 0.995
            self.learning_rate = 0.1
            self.model = self._build_model()

        def _build_model(self):

            # Neural Net for Deep-Q learning Model
            model = Sequential()
            model.add(Dense(self.action_size, input_dim=self.state_size, activation='linear'))
            #model.add(Dense(self.action_size, activation='linear'))
            model.compile(loss='mse',
                          optimizer=Adam(lr=self.learning_rate))
            return model


        def act(self, state):

            act_values = self.model.predict(state)

            return act_values


    dqn_agent = DQNAgent(10,2)

    dqn_agent_w = dqn_agent.model.get_weights()

    # Converts np.array to TEXT when inserting
    sqlite3.register_adapter(np.ndarray, adapt_array)

    # Converts TEXT to np.array when selecting
    sqlite3.register_converter("array", convert_array)

    con = sqlite3.connect("DB/test.db", detect_types=sqlite3.PARSE_DECLTYPES)

    cur = con.cursor()

    cur.execute("drop table if exists test")

    cur.execute("create table test (arr array)")

    cur.execute("insert into test (arr) values (?)", (dqn_agent_w[0],))

    cur.execute("select arr from test")

    data = cur.fetchone()[0]

    random_input = np.random.random((1,10))

    act_values = dqn_agent.act(random_input)

    #dqn_agent_w[0] = np.random.random((10,2))
    dqn_agent_w[1] = np.random.random(2)

    dqn_agent.model.set_weights(dqn_agent_w)

    act_values2 = dqn_agent.act(random_input)

    print("Done testing dqn")

#######################
## Run SQL lite3      #
#######################
run_sql_lite3 = 0
if run_sql_lite3:

    import sqlite3 as sql
    import numpy as np
    import json

    con = sql.connect('test.db', isolation_level=None)
    cur = con.cursor()
    cur.execute("DROP TABLE IF EXISTS FOOBAR")
    cur.execute("CREATE TABLE foobar (id INTEGER PRIMARY KEY, array BLOB)")
    cur.execute("INSERT INTO foobar VALUES (?,?)", (None, json.dumps(np.arange(0, 500, 0.5).tolist())))
    con.commit()
    cur.execute("SELECT * FROM FOOBAR")
    data = cur.fetchall()
    print(data)
    data = cur.fetchall()
    my_list = json.loads(data[0][1])

###############################
## Run AKQ ES policy only     #
###############################
run_akq_es_policy = 0
if run_akq_es_policy:

    print("Running AKQ ES")

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    tree.nodes[2].is_leaf = True

    es_agent = ESAgentPolicy(tree)

    es_agent.run_ES(100)

    behaviour_sb_matrix = {}
    behaviour_bb_matrix = {}

    # First SB

    for hand in [3,2,1]:

        s = es_agent.game_tree.nodes[0]

        es_agent.player1.current_hand = hand

        current_state = es_agent.get_state_vector(akq_game.tree.nodes[0],es_agent.player1)

        hand_string = es_agent.get_hand_string(hand)

        current_player = es_agent.player1

        current_agent = es_agent.sb_dqn_agent

        current_state = es_agent.get_state_vector(s, current_player)

        possible_actions = current_agent.act(current_state)[0]

        #legal_actions = es_agent.get_legal_actions(s)

        #legal_actions_string = [es_agent.actions[l] for l in legal_actions]

        #possible_actions = [possible_actions[i] for i in range(4) if i in legal_actions]

        possible_actions = list(map(abs,possible_actions))

        possible_actions = [action / sum(possible_actions) for action in possible_actions]

        legal_actions_string = ["bet","check"]

        behaviour_sb_matrix[hand_string] = {}

        behaviour_sb_matrix[hand_string][legal_actions_string[0]] = possible_actions[0]
        behaviour_sb_matrix[hand_string][legal_actions_string[1]] = possible_actions[1]

    # Next BB

    for hand in [3, 2, 1]:

        s = es_agent.game_tree.nodes[1]

        es_agent.player2.current_hand = hand

        current_state = es_agent.get_state_vector(akq_game.tree.nodes[1], es_agent.player2)

        hand_string = es_agent.get_hand_string(hand)

        current_player = es_agent.player2

        current_agent = es_agent.bb_dqn_agent

        current_state = es_agent.get_state_vector(s, current_player)

        possible_actions = current_agent.act(current_state)[0]

        #legal_actions = es_agent.get_legal_actions(s)

        #legal_actions_string = [es_agent.actions[l] for l in legal_actions]

        #possible_actions = [possible_actions[i] for i in range(4) if i in legal_actions]

        possible_actions = list(map(abs,possible_actions))

        possible_actions = [action / sum(possible_actions) for action in possible_actions]

        behaviour_bb_matrix[hand_string] = {}

        legal_actions_string = ['call','fold']

        behaviour_bb_matrix[hand_string][legal_actions_string[0]] = possible_actions[0]
        behaviour_bb_matrix[hand_string][legal_actions_string[1]] = possible_actions[1]


    print("SB policy: " + str(behaviour_sb_matrix) + "\n")
    print("BB policy: " + str(behaviour_bb_matrix) + "\n")
    print("Finished AKQ ES")

###############################
## Run AKQ ES value + policy  #
###############################
run_akq_es = 0
if run_akq_es:

    print("Running AKQ ES")

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})

    akq_game.new_action(current_index=1, player="BB", action={"call": 1})
    akq_game.new_action(current_index=1, player="BB", action={"fold": 0})

    tree.nodes[2].is_leaf = True

    es_agent = ESAgent(tree)

    es_agent.run_ES(10)

    print("Finished AKQ ES")

###############################
## Run ES simple half street  #
###############################
run_akq_es_policy = 0
if run_akq_es_policy:

    print("Running AKQ ES")

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})
    akq_game.new_action(current_index=1, player="BB", action={"call": 1})

    tree.nodes[2].is_leaf = True

    es_agent = ESAgentHalfStreet(tree)

    es_agent.run_ES(1000)

    behaviour_sb_matrix = {}
    behaviour_bb_matrix = {}

    # First SB

    for hand in [3,2,1]:

        s = es_agent.game_tree.nodes[0]

        es_agent.player1.current_hand = hand

        current_state = es_agent.get_state_vector(akq_game.tree.nodes[0],es_agent.player1)

        hand_string = es_agent.get_hand_string(hand)

        current_player = es_agent.player1

        current_agent = es_agent.sb_dqn_agent

        current_state = es_agent.get_state_vector(s, current_player)

        possible_actions = current_agent.act(current_state)[0]

        #legal_actions = es_agent.get_legal_actions(s)

        #legal_actions_string = [es_agent.actions[l] for l in legal_actions]

        #possible_actions = [possible_actions[i] for i in range(4) if i in legal_actions]

        possible_actions = list(map(abs,possible_actions))

        possible_actions = [action / sum(possible_actions) for action in possible_actions]

        legal_actions_string = ["bet","check"]

        behaviour_sb_matrix[hand_string] = {}

        behaviour_sb_matrix[hand_string][legal_actions_string[0]] = possible_actions[0]
        behaviour_sb_matrix[hand_string][legal_actions_string[1]] = possible_actions[1]


    print("SB policy: " + str(behaviour_sb_matrix) + "\n")

    print("Finished AKQ ES")

###############################
## Run fsp simple half street  #
###############################
run_fsp_half_street = 1
if run_fsp_half_street:

    print("Running AKQ ES")

    tree = Tree.Tree()

    players = ["SB", "BB"]

    init_SB_cip = 0.0
    init_BB_cip = 0.0

    akq_game = Game.GameState(tree=tree, players=players, name='akq_game')

    akq_game.set_root(players[0], init_SB_cip, init_BB_cip)

    root = akq_game.tree.get_root()

    akq_game.new_action(current_index=0, player="SB", action={"bet": 1})
    akq_game.new_action(current_index=0, player="SB", action={"check": 0})
    akq_game.new_action(current_index=1, player="BB", action={"call": 1})

    new_graph = gv.Digraph(format="png")

    AKQGraph = TreeGraph(tree=akq_game.tree, graph=new_graph)

    AKQGraph.create_graph_from_tree()

    AKQGraph.graph.render('/Users/befeltingu/GameTheory/Results/Poker/MCTS/img/akq_half')

    tree.nodes[2].is_leaf = True

    conn = sqlite3.connect('model_poker.db')

    for i in range(3):

        es_agent = FSPHalfStreetAgent(tree, conn)

        es_agent.run_fsp(250*i + 250)

        print('Num iterations: ' + str(250*i + 250))

        print(es_agent.sb_policy)


    print("Finished fsp simple half street")







