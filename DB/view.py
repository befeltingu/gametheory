import pandas as pd


#######################
# VIEW DF DATA
#######################
run_view_df_data = 1
if run_view_df_data:

    memory_df = pd.read_hdf('/Users/befeltingu/GameTheory/DB/fsp_half_street/memory_df')

    Ace_val_bet = memory_df[(memory_df['current_hand'] == 'A') & (memory_df['action'] == 'bet')].mean()
    Ace_val_check = memory_df[(memory_df['current_hand'] == 'A') & (memory_df['action'] == 'check')].mean()

    King_val_bet = memory_df[(memory_df['current_hand'] == 'K') & (memory_df['action'] == 'bet')].mean()
    King_val_check = memory_df[(memory_df['current_hand'] == 'K') & (memory_df['action'] == 'check')].mean()

    Queen_val_bet = memory_df[(memory_df['current_hand'] == 'Q') & (memory_df['action'] == 'bet')].mean()
    Queen_val_check = memory_df[(memory_df['current_hand'] == 'Q') & (memory_df['action'] == 'check')].mean()

    print("Done viewonig df")